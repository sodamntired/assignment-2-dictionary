import os, sys

_, f = sys.argv
os.system("make program >/dev/null")

tests = {
    "soooootired": "valid test without any tricks",
    "": "Element not found",
    "asdfasd" : "Element not found",
    "insomnia": "prog langs labs mood",
    "soooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooodamntired": "Key is invalid"
}

for i, v2 in enumerate(tests):
    print('Test', i + 1)
    with open("test-input", "w") as file:
        file.write(v2)
    v1 = os.popen(f"./{f} < test-input 2>&1").read().strip()
    if v1 != tests[v2]:
        print("Failed test:")
        print(v2)
        print(f'Output of {f}:')
        print(v1)
        print(f'Answer:')
        print(tests[v2])
        break
