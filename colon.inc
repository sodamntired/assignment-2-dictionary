%define setc 0
%macro colon 2
    %2: dq setc
        db %1, 0
    %define setc %2
%endmacro
